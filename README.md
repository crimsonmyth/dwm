# Dynamic Window Manager - Dwm
[Dwm](https://dwm.suckless.org/) is a dynamic window manager for X11 from the chaps at [suckless](https://suckless.org/).
Dwm is configured by editing C source code, and recompiling. The suckless website states that the project focuses on 
advanced and experienced computer users, and - perhaps tongue in cheek - that customization through editing source code "keeps
its userbase small and elitist".

## Patches
- Vanity Gaps.
- Actualfullscreen.

## Prerequisites
- Xorg-Server
- Xorg-Xinit
- Git
Some Xlib Header Files:
- XProto 
- Xft 
- Xinerama

## Downloading. 
Clone the repository:
```bash
git clone https://gitlab.com/crimsonmyth/dwm.git
```
## Installing. 
Install through the following manner:
```bash
cd dwm/
sudo make clean insall
```

## Running With Startx/Xinit. 
```bash
echo "exec dwm" >> ~/.xinitrc
```

## Running With a Display Manager. 
```bash
sudo touch /usr/share/xsessions/dwm.desktop
```
Add the following to dwm.desktop using your favourite text editor:
```bash
[Desktop Entry]
Encoding=UTF-8
Name=Dwm
Comment=the dynamic window manager
Exec=dwm
Icon=dwm
Type=XSession
```
You can now switch to dwm on your display manager by changing your session on your display manager. It is probably located on a 
hamburger menu or something. 
